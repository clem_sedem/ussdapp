
class Contact < ActiveRecord::Base
  # self.table_name = 'contacts'
end

class UssdLog < ActiveRecord::Base
  # self.table_name = 'ussd_logs'
end

class Tracker < ActiveRecord::Base
  # self.table_name = 'trackers'
end

class TmpContact < ActiveRecord::Base

end


class ConfirmDelete < ActiveRecord::Base
  self.table_name = 'confirm_delete'
end

class ConfirmEdit < ActiveRecord::Base
  self.table_name = 'confirm_edit'
end

class ConfirmShow < ActiveRecord::Base
  self.table_name = 'confirm_show'
end

