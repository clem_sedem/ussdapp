-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: clemencedb
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `confirm_delete`
--

DROP TABLE IF EXISTS `confirm_delete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `confirm_delete` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(50) DEFAULT NULL,
  `mobile_num` varchar(20) DEFAULT NULL,
  `choice` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `confirm_delete`
--

LOCK TABLES `confirm_delete` WRITE;
/*!40000 ALTER TABLE `confirm_delete` DISABLE KEYS */;
INSERT INTO `confirm_delete` VALUES (1,'35076827','233248152204','2'),(2,'35078463','233248152204','1'),(3,'35078929','233248152204','3'),(4,'35079679','233248152204','2'),(5,'35080681','233248152204','3'),(6,'35081247','233248152204','2'),(7,'35081749','233248152204','3'),(8,'35081949','233248152204','1'),(9,'35082231','233248152204','2'),(10,'35483194','233248152204','3');
/*!40000 ALTER TABLE `confirm_delete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `confirm_edit`
--

DROP TABLE IF EXISTS `confirm_edit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `confirm_edit` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(20) DEFAULT NULL,
  `mobile_num` varchar(15) DEFAULT NULL,
  `choice` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `confirm_edit`
--

LOCK TABLES `confirm_edit` WRITE;
/*!40000 ALTER TABLE `confirm_edit` DISABLE KEYS */;
INSERT INTO `confirm_edit` VALUES (1,'35443690','233248152204','3'),(2,'35444125','233248152204','1'),(3,'35445309','233248152204','2'),(4,'35445879','233248152204','2'),(5,'35446097','233248152204','2'),(6,'35446980','233248152204','2'),(7,'35447939','233248152204','2'),(8,'35448631','233248152204','3'),(9,'35449918','233248152204','3'),(10,'35451314','233248152204','3'),(11,'35451888','233248152204','3'),(12,'35453406','233248152204','3'),(13,'35454048','233248152204','3'),(14,'35454949','233248152204','3'),(15,'35455717','233248152204','3'),(16,'35457134','233248152204','3'),(17,'35458162','233248152204','3'),(18,'35460410','233248152204','1'),(19,'35460866','233248152204','2'),(20,'35468077','233248152204','3'),(21,'35469173','233248152204','3'),(22,'35481096','233248152204','3'),(23,'35482032','233248152204','3'),(24,'35505438','233248152204','2');
/*!40000 ALTER TABLE `confirm_edit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` varchar(20) DEFAULT NULL,
  `LASTNAME` varchar(20) DEFAULT NULL,
  `PHONE_NUM` varchar(13) DEFAULT NULL,
  `MOBILE_NUMBER` varchar(20) DEFAULT NULL,
  `AGE` int(20) DEFAULT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `STATUS` tinyint(1) DEFAULT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'Clem','Sedem','02937585994','233248152204',75,'m',1,'2017-08-15 17:08:29','2017-08-15 17:08:29'),(2,'Frans','Nartey','94847749939','233248152204',56,'m',1,'2017-08-15 17:09:42','2017-08-15 17:09:42'),(3,'Freda','Free','0247487474','233248152204',66,'f',0,'2017-08-15 17:10:46','2017-08-17 14:26:59');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_contacts`
--

DROP TABLE IF EXISTS `tmp_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_contacts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` varchar(20) DEFAULT NULL,
  `LASTNAME` varchar(20) DEFAULT NULL,
  `PHONE_NUM` varchar(13) DEFAULT NULL,
  `AGE` int(20) DEFAULT NULL,
  `GENDER` varchar(10) DEFAULT NULL,
  `MOBILE_NUM` varchar(15) DEFAULT NULL,
  `STATUS` tinyint(1) DEFAULT NULL,
  `SESSION_ID` varchar(50) DEFAULT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_contacts`
--

LOCK TABLES `tmp_contacts` WRITE;
/*!40000 ALTER TABLE `tmp_contacts` DISABLE KEYS */;
INSERT INTO `tmp_contacts` VALUES (1,'ho','gf','90',1,'m','233248152204',1,'35226403','2017-08-15 17:07:53','2017-08-17 12:20:46'),(2,'Fg','fgy','54',100,'m','233248152204',1,'35226557','2017-08-15 17:09:01','2017-08-17 16:46:02'),(3,'f','j','5',56,'m','233248152204',1,'35226713','2017-08-15 17:10:08','2017-08-17 14:19:37');
/*!40000 ALTER TABLE `tmp_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trackers`
--

DROP TABLE IF EXISTS `trackers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trackers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` int(11) DEFAULT NULL,
  `function` varchar(50) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `mobile_num` varchar(225) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `upated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trackers`
--

LOCK TABLES `trackers` WRITE;
/*!40000 ALTER TABLE `trackers` DISABLE KEYS */;
INSERT INTO `trackers` VALUES (1,9,'phone updated','35365602','233248152204',1,'2017-08-16 17:58:43','2017-08-16 18:00:09'),(2,10,'age selected','35367515','233248152204',1,'2017-08-16 18:14:09','2017-08-16 18:15:27'),(3,10,'age selected','35368296','233248152204',1,'2017-08-16 18:21:15','2017-08-16 18:22:33'),(4,7,'phone number','35371548','233248152204',1,'2017-08-16 18:47:28','2017-08-16 18:48:28'),(5,10,'age selected','35373903','233248152204',1,'2017-08-16 19:04:03','2017-08-16 19:05:25'),(6,11,'age updated','35374411','233248152204',1,'2017-08-16 19:07:20','2017-08-16 19:08:31'),(7,11,'age updated','35374901','233248152204',1,'2017-08-16 19:10:25','2017-08-16 19:11:46'),(8,11,'age updated','35376666','233248152204',1,'2017-08-16 19:21:22','2017-08-16 19:22:53'),(9,12,'summary','35379770','233248152204',1,'2017-08-16 19:41:35','2017-08-16 19:43:31'),(10,7,'phone number','35380577','233248152204',1,'2017-08-16 19:46:59','2017-08-16 19:47:53'),(11,12,'edit gender','35380853','233248152204',1,'2017-08-16 19:49:09','2017-08-16 19:50:45'),(12,13,'summary','35383968','233248152204',1,'2017-08-16 20:10:53','2017-08-16 20:12:50'),(13,6,'confirm to edit age','35424420','233248152204',1,'2017-08-17 08:18:55','2017-08-17 08:19:32'),(14,6,'confirm to edit age','35424809','233248152204',1,'2017-08-17 08:22:43','2017-08-17 08:23:31'),(15,7,'edit age','35425268','233248152204',1,'2017-08-17 08:27:11','2017-08-17 08:28:09'),(16,7,'edit age','35425966','233248152204',1,'2017-08-17 08:33:57','2017-08-17 08:34:35'),(17,7,'edit age','35426483','233248152204',1,'2017-08-17 08:39:13','2017-08-17 08:39:54'),(18,3,'confirm to edit firstname','35426767','233248152204',1,'2017-08-17 08:42:13','2017-08-17 08:42:31'),(19,7,'edit age','35426998','233248152204',1,'2017-08-17 08:44:26','2017-08-17 08:45:14'),(20,2,'select to edit','35427376','233248152204',1,'2017-08-17 08:48:08','2017-08-17 08:48:14'),(21,8,'age updated','35427711','233248152204',1,'2017-08-17 08:50:52','2017-08-17 08:51:38'),(22,7,'edit age','35428195','233248152204',1,'2017-08-17 08:55:13','2017-08-17 08:56:04'),(23,8,'age updated','35429194','233248152204',1,'2017-08-17 09:04:00','2017-08-17 09:05:00'),(24,7,'edit age','35431392','233248152204',1,'2017-08-17 09:22:29','2017-08-17 09:23:40'),(25,6,'confirm to edit age','35431654','233248152204',1,'2017-08-17 09:24:32','2017-08-17 09:25:10'),(26,7,'edit age','35432146','233248152204',1,'2017-08-17 09:27:32','2017-08-17 09:28:36'),(27,7,'confirm to edit gender','35432410','233248152204',1,'2017-08-17 09:29:14','2017-08-17 09:30:08'),(28,8,'age updated','35432977','233248152204',1,'2017-08-17 09:32:59','2017-08-17 09:33:46'),(29,7,'confirm to edit gender','35434643','233248152204',1,'2017-08-17 09:43:38','2017-08-17 09:44:26'),(30,7,'confirm to edit gender','35435042','233248152204',1,'2017-08-17 09:46:09','2017-08-17 09:46:51'),(31,8,'edit gender','35435657','233248152204',1,'2017-08-17 09:50:10','2017-08-17 09:50:56'),(32,4,'confirm to edit lastname','35436049','233248152204',1,'2017-08-17 09:52:57','2017-08-17 09:53:15'),(33,8,'edit gender ','35436292','233248152204',1,'2017-08-17 09:54:40','2017-08-17 09:55:26'),(34,3,'confirm to edit firstname','35437321','233248152204',1,'2017-08-17 10:01:36','2017-08-17 10:01:50'),(35,7,'confirm to edit gender','35437386','233248152204',1,'2017-08-17 10:02:02','2017-08-17 10:02:44'),(36,8,'edit gender','35438140','233248152204',1,'2017-08-17 10:06:35','2017-08-17 10:07:20'),(37,7,'confirm to edit gender','35440456','233248152204',1,'2017-08-17 10:20:14','2017-08-17 10:21:01'),(38,8,'edit gender','35440808','233248152204',1,'2017-08-17 10:22:12','2017-08-17 10:23:17'),(39,8,'show summay','35441448','233248152204',1,'2017-08-17 10:26:09','2017-08-17 10:27:07'),(40,9,'gender updated','35442627','233248152204',1,'2017-08-17 10:32:47','2017-08-17 10:33:50'),(41,10,'age selected','35443690','233248152204',1,'2017-08-17 10:39:35','2017-08-17 10:40:43'),(42,1,'menu','35443905','233248152204',1,'2017-08-17 10:40:58','2017-08-17 10:40:58'),(43,8,'phone selected','35444125','233248152204',1,'2017-08-17 10:42:19','2017-08-17 10:43:50'),(44,8,'phone selected','35445309','233248152204',1,'2017-08-17 10:50:05','2017-08-17 10:51:29'),(45,8,'phone selected','35445879','233248152204',1,'2017-08-17 10:54:06','2017-08-17 10:54:52'),(46,7,'phone number','35446097','233248152204',1,'2017-08-17 10:55:26','2017-08-17 10:56:18'),(47,8,'decide','35446980','233248152204',1,'2017-08-17 11:01:21','2017-08-17 11:02:29'),(48,8,'phone selected','35447939','233248152204',1,'2017-08-17 11:07:18','2017-08-17 11:08:14'),(49,7,'phone number','35448631','233248152204',1,'2017-08-17 11:11:00','2017-08-17 11:11:48'),(50,8,'edit phone','35449918','233248152204',1,'2017-08-17 11:18:26','2017-08-17 11:19:16'),(51,6,'lastname_selected','35451314','233248152204',1,'2017-08-17 11:26:58','2017-08-17 11:27:36'),(52,7,'phone number','35451888','233248152204',1,'2017-08-17 11:30:37','2017-08-17 11:31:34'),(53,8,'phone selected','35453406','233248152204',1,'2017-08-17 11:39:36','2017-08-17 11:40:44'),(54,8,'phone selected','35454048','233248152204',1,'2017-08-17 11:42:58','2017-08-17 11:43:48'),(55,7,'phone number','35454949','233248152204',1,'2017-08-17 11:47:56','2017-08-17 11:48:38'),(56,9,'phone updated','35455717','233248152204',1,'2017-08-17 11:52:47','2017-08-17 11:54:10'),(57,8,'age edit','35457134','233248152204',1,'2017-08-17 12:00:58','2017-08-17 12:01:52'),(58,8,'age edit','35458162','233248152204',1,'2017-08-17 12:07:09','2017-08-17 12:08:08'),(59,10,'age selected','35460410','233248152204',1,'2017-08-17 12:19:51','2017-08-17 12:20:53'),(60,11,'age updated','35460866','233248152204',1,'2017-08-17 12:22:28','2017-08-17 12:23:35'),(61,2,'select to edit','35462952','233248152204',1,'2017-08-17 12:34:41','2017-08-17 12:34:48'),(62,5,'firstname uptated','35468077','233248152204',1,'2017-08-17 13:02:34','2017-08-17 13:03:00'),(63,9,'edit age','35469173','233248152204',1,'2017-08-17 13:08:19','2017-08-17 13:09:15'),(64,13,'summary','35481096','233248152204',1,'2017-08-17 14:13:19','2017-08-17 14:14:39'),(65,6,'lastname updated','35482032','233248152204',1,'2017-08-17 14:19:03','2017-08-17 14:19:37'),(66,2,'show all','35483129','233248152204',1,'2017-08-17 14:26:15','2017-08-17 14:26:24'),(67,4,'confirm_delete','35483194','233248152204',1,'2017-08-17 14:26:38','2017-08-17 14:26:59'),(68,2,'show all','35483268','233248152204',1,'2017-08-17 14:27:11','2017-08-17 14:27:18'),(69,1,'menu','35504666','233248152204',1,'2017-08-17 16:41:12','2017-08-17 16:41:12'),(70,6,'lastname_selected','35505438','233248152204',1,'2017-08-17 16:45:31','2017-08-17 16:46:08');
/*!40000 ALTER TABLE `trackers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ussd_logs`
--

DROP TABLE IF EXISTS `ussd_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ussd_logs` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `msg_type` varchar(50) DEFAULT NULL,
  `service_key` varchar(50) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `mobile_num` varchar(13) DEFAULT NULL,
  `ussd_body` varchar(50) DEFAULT NULL,
  `nwk_coke` varchar(30) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ussd_logs`
--

LOCK TABLES `ussd_logs` WRITE;
/*!40000 ALTER TABLE `ussd_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `ussd_logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-17 17:43:38
