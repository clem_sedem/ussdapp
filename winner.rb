
require 'sinatra'
require 'active_record'
require 'json'
require 'net/http'
require 'yaml'
require 'savon'
require 'faraday'
require './models/model'

ActiveRecord::Base.establish_connection(
    adapter:  'mysql2',
    host:     "localhost",
    username: "clemence",
    password: "$My09b03!",
    database: "clemencedb"
)

post '/' do
  request.body.rewind
  @winner=JSON.parse(request.body.read)
  json_vals = @winner
  puts json_vals
  msg_type=json_vals['msg_type']
  nw_code=json_vals['nw_code']
  sessionID=json_vals['session_id']
  service_key=json_vals['service_code']
  mobile_number=json_vals['msisdn']
  ussd_body=json_vals['ussd_body'].strip if json_vals.has_key?('ussd_body')
  
  from_vodafone = ["release","Session timeout.","User timeout.","User timeout","Session timeout"]
     if from_vodafone.include?(ussd_body) && service_key.blank?
      s_params = Hash.new
      #p "THIS IS THE ANNOYING SESSION ID:  "+sessionID
      s_params['session_id'] = sessionID
      s_params['service_code'] = service_key
      s_params['msisdn'] = mobile_number
      s_params['nw_code'] = nw_code
      s_params['msg_type'] = "2"
      s_params['ussd_body'] = ussd_body
      p "DID THIS PORTION OF CODE REALLY RUN?...."
      p s_params.to_json
  
    else
      ussd_logs(msg_type, service_key, sessionID, mobile_number, ussd_body, nw_code)
      process(msg_type,service_key,sessionID,mobile_number,ussd_body,nw_code)
 
    end
     
end


#add contact method
def addContact(u_body, params, m_num, field, pg, fn, s_id)
    if pg ==2 && field == 'fname'
      #proccess firstname
        if u_body == "1" or "00"
          params['msg_type']='1'
          params['ussd_body']= "Enter your first name:\n00. Back"
          track_func(s_id, pg, fn, m_num)

        else
          params['msg_type']='1'
          params['ussd_body']= "wrong option"
        end

    elsif pg ==3 && field == 'lname'
      if is_string?(u_body) or u_body == "00"
         tmp_contact(u_body, s_id, 'firstname', m_num, params)
          params['msg_type']='1'
          params['ussd_body']= "Enter your last name:\n00. Back"
          track_func(s_id, pg, fn, m_num)
          
        else
          params['msg_type'] = "1"
          params['ussd_body'] = "Please enter a valid name"
      end
      
      
      
    elsif pg == 4 && field == 'phone_num' 
       if is_string?(u_body)or u_body == "00"
          tmp_contact(u_body, s_id, 'lname', m_num, params)
        #process phone number
          params['msg_type']='1'
          params['ussd_body']= "Enter your phone number:\n00. Back"
          track_func(s_id, pg, fn, m_num)
          
       else
          params['msg_type'] = "1"
          params['ussd_body'] = "Please enter a valid name"
       end
      

    elsif pg == 5 && field == 'age'
      if mobile_number_matched?(u_body) or u_body == "00"
        tmp_contact(u_body, s_id, 'p_num', m_num, params)
        #proccess age
        params['msg_type']='1'
        params['ussd_body']= "Enter your age:\n00. Back"   
        track_func(s_id, pg, fn, m_num)
        
      else
        params['msg_type'] = "1"
        params['ussd_body'] = "Please enter a valid phone number"
      end
      

    elsif pg == 6 && field == 'gender'
      if is_number?(u_body) or u_body == "00" 
          tmp_contact(u_body, s_id, 'age', m_num, params)
          #process gender
          params['msg_type']='1'
          params['ussd_body']= "Select your gender\n1. Male\n2. Female\n00. Back"
          track_func(s_id, pg, fn, m_num)
          
       else
          params['msg_type'] = "1"
          params['ussd_body'] = "Please enter a valid age"
      end
      

      elsif pg == 7 && field == 'done'
        if u_body.to_i >= 3 
          params['msg_type'] = "1"
          params['ussd_body'] = "Please select 1 for male or 2 for female"
          
        else
          tmp_contact(u_body, s_id, 'gender', m_num, params)
          track_func(s_id, pg, fn, m_num)
          display_contact(s_id, params)
       end
      

    elsif pg == 8 && field =='inserted'
      if u_body == '1'
        insert_contact(s_id, params, m_num)
        track_func(s_id, pg, fn, m_num)
      else
        params['msg_type'] = '2'
        params['ussd_body'] = 'You have successfully cancelld the registration'
      end

    end
    p params.to_json

end


#contacts show
def contactShow(u_body, params, m_num, field, pg, fn, s_id)
  if pg == 2 && field == "show contact"
    if u_body == "3"
      track_func(s_id, pg, fn, m_num)
      show_contact(m_num, params)
      
    elsif u_body == "00"
      track_func(s_id, pg, fn, m_num)
      show_contact(m_num, params)
       
    else
      params['msg_type'] = '1'
      params['ussd_body'] = 'wrong option'
    end
    
    elsif pg == 3 && field == "show detail"
      track_func(s_id, pg, fn, m_num)
      tmp_show(u_body, s_id, m_num, params)
      contact_chosen_show(s_id, m_num, params, u_body)
          
  end
  p params.to_json
end

#Edit contact
def contactEdit(u_body, params, m_num, field, pg, fn, s_id)
  if pg == 2 && field == "edit contact"
    if u_body == "2"
      track_func(s_id, pg, fn, m_num)
      show_contact_edit(m_num, params)
      
    elsif u_body == "00"
      track_func(s_id, pg, fn, m_num)
      show_contact_edit(m_num, params)
      
    else
      params['msg_type'] = '1'
      params['ussd_body'] = 'wrong option'
    end

  elsif pg == 3 && field == "choose"
    track_func(s_id, pg, fn, m_num)
    tmp_edit(u_body,s_id, m_num, params)
    contact_chosen(s_id, m_num, params, u_body)

  elsif pg == 4 && field == "edit firstname"
    track_func(s_id, pg, fn, m_num)
    enter_new_firstname(params)


  elsif pg == 4 && field == "edit lastname"
    track_func(s_id, pg, fn, m_num)
    enter_new_lastname(params)

  elsif pg == 4 && field == "edit phone number"
    track_func(s_id, pg, fn, m_num)
    enter_new_phone_number(params)

  elsif pg == 4 && field == "edit age"
    track_func(s_id, pg, fn, m_num)
    enter_new_age(params)

  elsif pg == 4 && field == "edit gender"
    track_func(s_id, pg, fn, m_num)
    enter_new_gender(params)

  elsif pg ==4 && field == "invalid selection"
    params['msg_type'] = "1"
    params['ussd_body'] = "Invalid Selection"


  elsif pg == 5 && field =="fname saved/wish to continue?"
    if is_string?(u_body)
      track_func(s_id, pg, fn, m_num)
      new_firstname(s_id, m_num, u_body)
      firstname_saved_prompt(params)
      
    else 
      params['msg_type'] = "1"
      params['ussd_body'] = "Please enter a valid name"
    end
    


  elsif pg == 5 && field == "lname saved/wish to continue?"
    if is_string?(u_body)
      track_func(s_id, pg, fn, m_num)
      new_lastname(s_id, m_num, u_body)
      lastname_saved_prompt(params)
      
    else
      params['msg_type'] = "1"
      params['ussd_body'] = "Please enter a valid name"
    end
    

  elsif pg == 5 && field == "phone number saved/wish to continue?"
    if mobile_number_matched?(u_body)
      track_func(s_id, pg, fn, m_num)
      new_phone_number(s_id, m_num, u_body)
      phone_number_saved_prompt(params)
      
    else
      params['msg_type'] = "1"
      params['ussd_body'] = "Please enter a valid phone number"
      
    end
    

  elsif pg == 5 && field == "age saved/wish to continue?"
    if is_number?(u_body)
      track_func(s_id, pg, fn, m_num)
      new_age(s_id, m_num, u_body)
      age_saved_prompt(params)
      
    else  
      params['msg_type'] = "1"
      params['ussd_body'] = "Please enter a valid age"
    end
    

  elsif pg == 5 && field == "gender saved/wish to continue?"
    if u_body.to_i >= 3
      params['msg_type'] = "1"
      params['ussd_body'] = "Please 1 for male or 2 for female"
      
    else
      track_func(s_id, pg, fn, m_num)
      new_gender(s_id, m_num, u_body, params)
      gender_saved_prompt(params)
    end 
      
  elsif pg == 6 && field == "restart edit"
    track_func(s_id, pg, fn, m_num)
    contact_chosen(s_id, m_num, params, u_body)


  elsif pg == 1 && field == "back to menu"
    track_func(s_id, pg, fn, m_num)
    menu(params, s_id, m_num)

  end
 
  p params.to_json
end


#Delete contact
def deleteContact(u_body, params, m_num, field, pg, fn, s_id)
  if pg == 2 && field == "del_show"
    if u_body == "4"
      track_func(s_id, pg, fn, m_num)
      show_contact_delete(m_num, params)
    else
      params['msg_type']='1'
      params['ussd_body']= "wrong option"
    end

  elsif pg == 3 && field == "delete"
    track_func(s_id, pg, fn, m_num)
    delete(u_body, m_num, params, s_id)

  elsif pg == 4 && field == "deleted"
    track_func(s_id, pg, fn, m_num)
    confirm_delete(u_body, m_num, params, s_id)

  end

  p params.to_json
end


def menu(params, s_id, m_num)
  pg = 1
  fn = 'menu'
  str= "Welcome to the Address Book \n Please select an option! \n"
  str<<"1: Add Contact \n"
  str<<"2: Edit Contact \n"
  str<<"3: Display Contact \n"
  str<<"4: Delete Contact"
  # return str
  track_func(s_id, pg, fn, m_num)

  params['msg_type']='1'
  params['ussd_body']= str
  p params.to_json
end

#validate character
def is_string?(value)
    value.match(/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u)
end

#validate phone number
def mobile_number_matched?(mobile_number)
    mobile_number.match(/^(\+?)(\d){10,12}$/)
end

#validate age
def is_number?(value)
  # true if Float(self) rescue false
  value.match(/\A[-+]?[0-9]*\.?[0-9]+\Z/)
end


def track_func(s_id, pg, fn, m_num)
  monitor = Tracker.where(session_id:s_id).last
  if monitor
    monitor.page = pg
    monitor.function = fn
    monitor.save
  else
    Tracker.create!(page:pg, function:fn, session_id:s_id, mobile_num:m_num, status:1 )
  end

end

def ussd_logs(msg_type, service_key, s_id, m_num, u_body, nw_code)
  UssdLog.create(msg_type:msg_type, service_key:service_key, session_id:s_id, mobile_num:m_num, ussd_body:u_body, nw_code:nw_code, status:1)
  
end


def tmp_edit(u_body, s_id, m_num, params)
  Contact.where(MOBILE_NUM:m_num).order('id asc')
  user_select = u_body.to_i
  if user_select <= 0
    params['msg_type'] = "1"
    params['ussd_body'] = "Invalid input"

  else
    ConfirmEdit.create(session_id:s_id, mobile_num:m_num, choice:u_body)
    contact_chosen(s_id, m_num, params, u_body)
    # params['msg_type'] = "1"
    # params['ussd_body'] = "Do you want to edit firstname?\n 1)Yes\n 2)No\n "

  end
end

def tmp_show(u_body, s_id, m_num, params)
  Contact.where(MOBILE_NUM:m_num).order('id asc')
  user_select = u_body.to_i
  if user_select <= 0
    params['msg_type'] = "1"
    params['ussd_body'] = "Invalid input"

  else
    ConfirmShow.create(session_id:s_id, mobile_num:m_num, choice:u_body)
    # contact_chosen(s_id, m_num, params)
    # params['msg_type'] = "1"
    # params['ussd_body'] = "Do you want to edit firstname?\n 1)Yes\n 2)No\n "

  end
end




#Enter new values promps
def enter_new_firstname(params)
  params['msg_type'] = "1"
  params['ussd_body'] = "Please enter your new firstname"
end

def enter_new_lastname(params)
  params['msg_type'] = "1"
  params['ussd_body'] = "Please enter your new lastname"
end

def enter_new_phone_number(params)
  params['msg_type'] = "1"
  params['ussd_body'] = "Please enter your new phone number"
end

def enter_new_age(params)
  params['msg_type'] = "1"
  params['ussd_body'] = "Please enter your new age"
end

def enter_new_gender(params)
  params['msg_type'] = "1"
  params['ussd_body'] = "Please select your gender\n1. Male\n2. Female"
end

#updated promps
def firstname_saved_prompt(params)
  disp = "Firstname updated successfully\n\n"
  disp << "Do you want to perform another edit?\n 1)Yes\n 2)No\n "
  params['msg_type'] = "1"
  params['ussd_body'] = disp
end

def lastname_saved_prompt(params)
  disp = "Lastname updated successfully\n\n"
  disp << "Do you want to perform another edit?\n 1)Yes\n 2)No\n "
  params['msg_type'] = "1"
  params['ussd_body'] = disp
end

def phone_number_saved_prompt(params)
  disp = "Phone number updated successfully\n\n"
  disp << "Do you want to perform another edit?\n 1)Yes\n 2)No\n "
  params['msg_type'] = "1"
  params['ussd_body'] = disp
end

def age_saved_prompt(params)
  disp = "Age updated successfully\n\n"
  disp << "Do you want to perform another edit?\n 1)Yes\n 2)No\n "
  params['msg_type'] = "1"
  params['ussd_body'] = disp
end

def gender_saved_prompt(params)
  disp = "Gender  updated successfully\n\n"
  disp << "Do you want to perform another edit?\n 1)Yes\n 2)No\n "
  params['msg_type'] = "1"
  params['ussd_body'] = disp
end

#get the selected contact to edit
def contact_chosen(s_id, p_num, params, u_body)
  value = u_body.to_i
  contact = ConfirmEdit.where(session_id:s_id, choice:value).order('id asc')[0]
  selected_contact = contact.choice
  contacts = Contact.where(MOBILE_NUMBER:p_num, STATUS: true).order('id asc')
  new_cont = contacts[selected_contact.to_i - 1]
  # puts "here is the value: #{value}"
  num_contact = contacts.count
  
  if value > num_contact
    params['msg_type'] = "2"
    params['ussd_body'] = "Wrong Selection"
  else
    
  firstname = new_cont.FIRSTNAME
  lastname = new_cont.LASTNAME
  phone = new_cont.PHONE_NUM
  age = new_cont.AGE
  gender = new_cont.GENDER
  cont = "Please select what you wish to edit\n\n"
  cont << " 1. Firstname:  #{firstname}\n 2. Lastname: #{lastname}\n 3. Phone number:  #{phone}\n 4. Age:  #{age}\n 5. Gender:  #{gender}\n"
  params['msg_type'] = '1'
  params['ussd_body'] = cont + "00. Back"
  
  end

end

def contact_chosen_show(s_id, p_num, params, u_body)
  value = u_body.to_i
  contact = ConfirmShow.where(session_id:s_id, choice:value).order('id asc')[0]
  selected_contact = contact.choice
  contacts = Contact.where(MOBILE_NUMBER:p_num, STATUS: true).order('id asc')
  new_cont = contacts[selected_contact.to_i - 1]
  
  num_contact = contacts.count
  
  if value > num_contact
    params['msg_type'] = "2"
    params['ussd_body'] = "Wrong Selection"
    
    
  else
    firstname = new_cont.FIRSTNAME
    lastname = new_cont.LASTNAME
    phone = new_cont.PHONE_NUM
    age = new_cont.AGE
    gender = new_cont.GENDER
    cont = "Contact Details\n\n"
    cont << "Firstname:  #{firstname}\nLastname: #{lastname}\nPhone number:  #{phone}\nAge:  #{age}\nGender:  #{gender}\n"
    params['msg_type'] = '1'
    params['ussd_body'] = cont + "00. Back"
  
  end

end



#save new values
def new_firstname(s_id, p_num, u_body)
    value = u_body
    contact = ConfirmEdit.where(session_id:s_id).order('id asc')[0]
    selected_contact = contact.choice
    contacts = Contact.where(MOBILE_NUMBER:p_num, STATUS:true).order('id asc')
    new_cont = contacts[selected_contact.to_i - 1]
    new_cont.FIRSTNAME =  value
    # new_tmp.SESSION_ID = s_id
    new_cont.save

    # puts "Here my selected contact: #{@new.inspect}"

end

def new_lastname(s_id, p_num, u_body)
  value = u_body
  contact = ConfirmEdit.where(session_id:s_id).order('id asc')[0]
  selected_contact = contact.choice
  contacts = Contact.where(MOBILE_NUMBER:p_num, STATUS:true).order('id asc')
  new_cont = contacts[selected_contact.to_i - 1]
  new_cont.LASTNAME =  value
  # new_tmp.SESSION_ID = s_id
  new_cont.save
  # puts "Here my selected contact: #{new_tmp.inspect}"
end

def new_phone_number(s_id, p_num, u_body)
  value = u_body
  contact = ConfirmEdit.where(session_id:s_id).order('id asc')[0]
  selected_contact = contact.choice
  contacts = Contact.where(MOBILE_NUMBER:p_num, STATUS:true).order('id asc')
  new_cont = contacts[selected_contact.to_i - 1]
  new_cont.PHONE_NUM =  value
  # new_tmp.SESSION_ID = s_id
  new_cont.save
  # puts "Here my selected contact: #{new_tmp.inspect}"
end

def new_age(s_id, p_num, u_body)
  value = u_body
  contact = ConfirmEdit.where(session_id:s_id).order('id asc')[0]
  selected_contact = contact.choice
  contacts = Contact.where(MOBILE_NUMBER:p_num, STATUS:true).order('id asc')
  new_cont = contacts[selected_contact.to_i - 1]
  new_cont.AGE =  value
  # new_tmp.SESSION_ID = s_id
  new_cont.save
  # puts "Here my selected contact: #{new_tmp.inspect}"
end

def new_gender(s_id, p_num, u_body, params)
  value = u_body.to_i
  if value == 1
    gender = "Male"
    contact = ConfirmEdit.where(session_id:s_id).order('id asc')[0]
    selected_contact = contact.choice
    contacts = Contact.where(MOBILE_NUMBER:p_num, STATUS:true).order('id asc')
    new_cont = contacts[selected_contact.to_i - 1]
    new_cont.GENDER =  gender
    # new_tmp.SESSION_ID = s_id
    new_cont.save
    # puts "Here my selected contact: #{new_tmp.inspect}"

  elsif value == 2
    gender = "Female"
    contact = ConfirmEdit.where(session_id:s_id).order('id asc')[0]
    selected_contact = contact.choice
    contacts = Contact.where(MOBILE_NUMBER:p_num, STATUS:true).order('id asc')
    new_cont = contacts[selected_contact.to_i - 1]
    new_cont.GENDER =  gender

    new_cont.save
    # puts "Here my selected contact: #{new_tmp.inspect}"

  else
    params['msg_type'] = '1'
    params['ussd_body'] = 'Invalid input'

  end


end

def new_phone_num(s_id, p_num, u_body)
  value = u_body
  contact = ConfirmEdit.where(session_id:s_id).order('id asc')[0]
  selected_contact = contact.choice
  tmpcontacts = TmpContact.where(MOBILE_NUM:p_num).order('id asc')
  new_tmp = tmpcontacts[selected_contact.to_i - 1]
  new_tmp.PHONE_NUM = value
  new_tmp.SESSION_ID = s_id
  new_tmp.save

  # puts "Here my selected contact: #{new_tmp.inspect}"
end


#get contact to delete
def delete(u_body, p_num, params, s_id)
  check = Contact.where(MOBILE_NUMBER:p_num, STATUS: true).order('id asc')
  user_select = u_body.to_i
  get_contact = user_select - 1
  fetch = check[get_contact]
  num_contact = check.count
  
  # puts "Total number of contacts saved: #{num_contact}"

  if user_select <= 0
     params['msg_type'] = "2"
     params['ussd_body'] = "Invalid input"
     
  elsif num_contact < user_select
     params['msg_type'] = "2"
     params['ussd_body'] = "Invalid wwww "

  else
    params['msg_type'] = "1"
    params['ussd_body'] = "Do you want to proceed?\n 1. Yes\n 2. No"
    ConfirmDelete.create(session_id:s_id, mobile_num:p_num, choice:u_body)

  end

end


#confirm actual delete
def confirm_delete(u_body, p_num, params, s_id)
  u_body= u_body.to_i
  if u_body == 1
    object = ConfirmDelete.where(session_id:s_id).order('id asc')[0]
     index = object.choice
    contacts = Contact.where(MOBILE_NUMBER:p_num, STATUS: true).order('id asc')
    selected_contact = contacts[index.to_i - 1]
    selected_contact.STATUS = false
    selected_contact.save
     # puts "My Selected Contact : #{selected_contact.inspect}"

    params['msg_type'] = "2"
    params['ussd_body'] = "Contact deleted successfully"

  elsif u_body == 2
    show_contact_delete(p_num, params)
  else
    params['msg_type'] = "1"
    params['ussd_body'] = "Invalid Input"
  end

end


def tmp_contact(u_body, s_id, field, m_num, params)
 if u_body != "00" 
  check = TmpContact.where(session_id:s_id,)[0]
  if check
    
    if field == 'lname'
      check.LASTNAME= u_body
      check.save
    elsif field == 'p_num'
      check.PHONE_NUM = u_body
      check.save
    elsif field == 'age'
    check.AGE = u_body
    check.save
    
    elsif field == 'gender' 
      
      gender = u_body.to_i
      
      if gender == 1
        value = "Male"
        check.GENDER = value
        check.MOBILE_NUM = m_num
        check.save

      elsif gender == 2
        value = "Female"
        check.GENDER = value
        check.MOBILE_NUM = m_num
        check.save
        
      end
          
          
          
          
    end

  else
    
    TmpContact.create!(FIRSTNAME:u_body, STATUS:1, SESSION_ID:s_id)
  end
  
  end
end



def insert_contact(s_id, params, m_num)
    temp = TmpContact.where(session_id:s_id).last
    firstname = temp.FIRSTNAME
    lastname = temp.LASTNAME
    phone = temp.PHONE_NUM
    age = temp.AGE
    gender = temp.GENDER
    Contact.create!(FIRSTNAME:firstname, LASTNAME:lastname, PHONE_NUM:phone, MOBILE_NUMBER:m_num, AGE:age, GENDER:gender, STATUS:1)
    params['msg_type'] = '2'
    params['ussd_body']= 'Contact saved successfully'

end

def display_contact(s_id, params)
  contact = TmpContact.where(session_id:s_id).last
  first = contact.FIRSTNAME
  last = contact.LASTNAME
  phone = contact.PHONE_NUM
  age = contact.AGE
  gender = contact.GENDER
  cont = "Contact Summary"
  cont << "\nFirst name: #{first}\nLast name: #{last}\nPhone number: #{phone}\nAge: #{age}\nGender: #{gender}\n1. Save\n2. Cancel\n00. Back"
  params['msg_type'] = '1'
  params['ussd_body'] = cont
end

def show_contact(p_num, params)
    cont = Contact.where(MOBILE_NUMBER:p_num, STATUS:true)
    show = "Please select a contact to edit!\n"
    none = "Sorry you do not have any contact saved!\n"
    cont.each_with_index do |contact, index|
      show << "#{index+1}. #{contact.FIRSTNAME} #{contact.LASTNAME}\n"
    end
    if cont.empty?
      params['msg_type'] = '2'
      params['ussd_body'] = none
    else
      params['msg_type'] = '1'
      params['ussd_body'] = show + "00. Back"
    end
end


def show_contact_edit(p_num, params)
    cont = Contact.where(MOBILE_NUMBER:p_num, STATUS:true)
    show = "Please select a contact to edit!\n"
    none = "Sorry you do not have any contact saved!\n"
    cont.each_with_index do |contact, index|
      show << "#{index+1}. #{contact.FIRSTNAME} #{contact.LASTNAME}\n"
    end
    if cont.empty?
      params['msg_type'] = '2'
      params['ussd_body'] = none
    else
      params['msg_type'] = '1'
      params['ussd_body'] = show + "00. Back"
    end
end


def edit_summary(s_id, params)
  contact = TmpContact.where(session_id:s_id).last
  first = contact.FIRSTNAME
  last = contact.LASTNAME
  phone = contact.PHONE_NUM
  age = contact.AGE
  gender = contact.GENDER
  cont = "Edited contact info!"
  cont << "\nFirst name: #{first}\nLast name: #{last}\nPhone number: #{phone}\nAge: #{age}\nGender: #{gender}\n1. Save\n2. Cancel"
  params['msg_type'] = '1'
  params['ussd_body'] = cont
end


def show_contact_delete(p_num, params)
  cont = Contact.where(MOBILE_NUMBER:p_num, STATUS:true)
  show = "Please select a contact to delete!\n"
  none = "Sorry you do not have any contact saved!\n"
  cont.each_with_index do |contact, index|
    show << "#{index+1}. #{contact.FIRSTNAME} #{contact.LASTNAME}\n"
  end

  if cont.empty?
    params['msg_type'] = '2'
    params['ussd_body'] = none
  else
    params['msg_type'] = '1'
    params['ussd_body'] = show + "00. Back"
  end

end

def process(msg_type,service_key,sessionID,mobile_number,ussd_body,network_code)
    s_params=Hash.new
    s_params['msg_type']=msg_type
    s_params['session_id']=sessionID
    s_params['service_code']=service_key
    s_params['msisdn']=mobile_number
    s_params['nw_code'] = network_code
    puts ussd_body

    if msg_type == "0"
      menu(s_params, sessionID, mobile_number )
       
    else
      trace = Tracker.where(session_id:sessionID).last
      if trace.function == 'menu' && trace.page == 1 && ussd_body == "1"
          pg = 2
          fn = 'firstname'
          addContact(ussd_body, s_params, mobile_number, 'fname', pg, fn, sessionID )
          
      #don't want to enter firstname but go back
      elsif trace.function == 'firstname' && trace.page == 2 && ussd_body == "00"
          menu(s_params, sessionID, mobile_number )
      #end
          
          
        #wrong input on home page
      elsif trace.function == 'menu' && trace.page == 1 && ussd_body.to_i >= 5
        s_params['msg_type'] = "2"
        s_params['ussd_body'] = "Wrong Selection"
        p s_params.to_json
       #end of home page wrong input
          
          
      #   if a user wants to edit contact
      elsif trace.function == 'menu' && trace.page == 1 && ussd_body == "2"
          pg = 2
          edit_fn = 'select to edit'
          contactEdit(ussd_body, s_params, mobile_number, 'edit contact', pg, edit_fn, sessionID )


      elsif trace.function == 'select to edit' && trace.page == 2 && ussd_body != "00"
        pg = 3
        s_fn = 'choose what to edit'
        contactEdit(ussd_body, s_params, mobile_number, 'choose', pg, s_fn, sessionID )
        
        #go back
       elsif trace.function == 'select to edit' && trace.page == 2 && ussd_body == "00"
         menu(s_params, sessionID, mobile_number )
        #end of go back         

      elsif trace.function == 'choose what to edit' && trace.page == 3 && ussd_body == "1"
        pg = 4
        s_fn = 'edit firstname'
        contactEdit(ussd_body, s_params, mobile_number, 'edit firstname', pg, s_fn, sessionID )

      elsif trace.function == 'choose what to edit' && trace.page == 3 && ussd_body == "2"
        pg = 4
        s_fn = 'edit lastname'
        contactEdit(ussd_body, s_params, mobile_number, 'edit lastname', pg, s_fn, sessionID )

      elsif trace.function == 'choose what to edit' && trace.page == 3 && ussd_body == "3"
        pg = 4
        s_fn = 'edit phone number'
        contactEdit(ussd_body, s_params, mobile_number, 'edit phone number', pg, s_fn, sessionID )

      elsif trace.function == 'choose what to edit' && trace.page == 3 && ussd_body == "4"
        pg = 4
        s_fn = 'edit age'
        contactEdit(ussd_body, s_params, mobile_number, 'edit age', pg, s_fn, sessionID )

      elsif trace.function == 'choose what to edit' && trace.page == 3 && ussd_body == "5"
        pg = 4
        s_fn = 'edit gender'
        contactEdit(ussd_body, s_params, mobile_number, 'edit gender', pg, s_fn, sessionID )
        
        #go back
        elsif trace.function == 'choose what to edit' && trace.page == 3 && ussd_body == "00"
          pg = 2
          edit_fn = 'select to edit'
          contactEdit(ussd_body, s_params, mobile_number, 'edit contact', pg, edit_fn, sessionID )
        #end of go back

      elsif trace.function == 'choose what to edit' && trace.page == 3 && ussd_body.to_i >= 6 
        pg = 4
        s_fn = 'invalid selection'
        contactEdit(ussd_body, s_params, mobile_number, 'invalid selection', pg, s_fn, sessionID )

      elsif trace.function == 'choose what to edit' && trace.page == 3 && ussd_body.to_i <= 0
        pg = 4
        s_fn = 'invalid selection'
        contactEdit(ussd_body, s_params, mobile_number, 'invalid selection', pg, s_fn, sessionID )




      elsif trace.function == 'edit firstname' && trace.page == 4
        pg = 5
        s_fn = 'fname saved/wish to continue?'
        contactEdit(ussd_body, s_params, mobile_number, 'fname saved/wish to continue?', pg, s_fn, sessionID )

      elsif trace.function == 'edit lastname' && trace.page == 4
        pg = 5
        s_fn = 'lname saved/wish to continue?'
        contactEdit(ussd_body, s_params, mobile_number, 'lname saved/wish to continue?', pg, s_fn, sessionID )

      elsif trace.function == 'edit phone number' && trace.page == 4
        pg = 5
        s_fn = 'phone number saved/wish to continue?'
        contactEdit(ussd_body, s_params, mobile_number, 'phone number saved/wish to continue?', pg, s_fn, sessionID )

      elsif trace.function == 'edit age' && trace.page == 4
        pg = 5
        s_fn = 'age saved/wish to continue?'
        contactEdit(ussd_body, s_params, mobile_number, 'age saved/wish to continue?', pg, s_fn, sessionID )

      elsif trace.function == 'edit gender' && trace.page == 4
        pg = 5
        s_fn = 'gender saved/wish to continue?'
        contactEdit(ussd_body, s_params, mobile_number, 'gender saved/wish to continue?', pg, s_fn, sessionID )




      elsif trace.function == 'fname saved/wish to continue?' && trace.page == 5 && ussd_body == "1"
        pg = 3
        s_fn = 'choose what to edit'
        contactEdit(ussd_body, s_params, mobile_number, 'choose', pg, s_fn, sessionID )

      elsif trace.function == 'lname saved/wish to continue?' && trace.page == 5 && ussd_body == "1"
        pg = 3
        s_fn = 'choose what to edit'
        contactEdit(ussd_body, s_params, mobile_number, 'choose', pg, s_fn, sessionID )

      elsif trace.function == 'phone number saved/wish to continue?' && trace.page == 5 && ussd_body == "1"
        pg = 3
        s_fn = 'choose what to edit'
        contactEdit(ussd_body, s_params, mobile_number, 'choose', pg, s_fn, sessionID )

      elsif trace.function == 'age saved/wish to continue?' && trace.page == 5 && ussd_body == "1"
        pg = 3
        s_fn = 'choose what to edit'
        contactEdit(ussd_body, s_params, mobile_number, 'choose', pg, s_fn, sessionID )

      elsif trace.function == 'gender saved/wish to continue?' && trace.page == 5 && ussd_body == "1"
        pg = 3
        s_fn = 'choose what to edit'
        contactEdit(ussd_body, s_params, mobile_number, 'choose', pg, s_fn, sessionID )






      elsif trace.function == 'fname saved/wish to continue?' && trace.page == 5 && ussd_body == "2"
        pg = 1
        s_fn = 'menu'
        contactEdit(ussd_body, s_params, mobile_number, 'back to menu', pg, s_fn, sessionID )

      elsif trace.function == 'lname saved/wish to continue?' && trace.page == 5 && ussd_body == "2"
        pg = 1
        s_fn = 'menu'
        contactEdit(ussd_body, s_params, mobile_number, 'back to menu', pg, s_fn, sessionID )

      elsif trace.function == 'phone number saved/wish to continue?' && trace.page == 5 && ussd_body == "2"
        pg = 1
        s_fn = 'menu'
        contactEdit(ussd_body, s_params, mobile_number, 'back to menu', pg, s_fn, sessionID )

      elsif trace.function == 'age saved/wish to continue?' && trace.page == 5 && ussd_body == "2"
        pg = 1
        s_fn = 'menu'
        contactEdit(ussd_body, s_params, mobile_number, 'back to menu', pg, s_fn, sessionID )

      elsif trace.function == 'gender saved/wish to continue?' && trace.page == 5 && ussd_body == "2"
        pg = 1
        s_fn = 'menu'
        contactEdit(ussd_body, s_params, mobile_number, 'back to menu', pg, s_fn, sessionID )


        #if a user wants to display contacts
      elsif trace.function == 'menu' && trace.page == 1 && ussd_body == "3"
        pg = 2
        shw_fn = 'show all'
        contactShow(ussd_body, s_params, mobile_number, 'show contact', pg, shw_fn, sessionID)
        
      elsif trace.function == 'show all' && trace.page == 2 && ussd_body != "00"
        pg = 3
        shw_fn = 'show detail'
        contactShow(ussd_body, s_params, mobile_number, 'show detail', pg, shw_fn, sessionID)
        
        #go back
      elsif trace.function == 'show all' && trace.page == 2 && ussd_body == "00"
        menu(s_params, sessionID, mobile_number )
        
      elsif trace.function == 'show detail' && trace.page == 3 && ussd_body == "00"
        pg = 2
        shw_fn = 'show all'
        contactShow(ussd_body, s_params, mobile_number, 'show contact', pg, shw_fn, sessionID) 
        #end of go back
        
        #wrong input at show contact detail
      elsif trace.function == 'show detail' && trace.page == 3 && ussd_body.to_i >= 1
        s_params['msg_type'] = "1"
        s_params['ussd_body'] = "Wrong Input"
        p s_params.to_json
        # end of wrong input at show contact detail
     
        
        # if a user wants to delete contact
      elsif trace.function == 'menu' && trace.page == 1 && ussd_body == "4" && ussd_body != "00"
        pg = 2
        del_fn = 'del'
        deleteContact(ussd_body, s_params, mobile_number, 'del_show', pg, del_fn, sessionID)
        
        #go back
      elsif trace.function == 'del' && trace.page == 2 && ussd_body == "00"
        menu(s_params, sessionID, mobile_number )
        
      elsif trace.function == 'confirm_delete' && trace.page == 4 && ussd_body == "00"
        menu(s_params, sessionID, mobile_number )
        #end of go back


      elsif trace.function == 'firstname'  && trace.page == 2 && ussd_body != "00" 
        pg = 3
        fn = 'lastname'
        addContact(ussd_body, s_params, mobile_number, 'lname', pg, fn, sessionID )
        
        
        elsif trace.function == 'lastname'  && trace.page == 3 && ussd_body == "00" 
        pg = 2
        fn = 'firstname'
        addContact(ussd_body, s_params, mobile_number, 'fname', pg, fn, sessionID )

        
        
        
      elsif trace.function == 'del' && trace.page == 2
        pg = 3
        del_fn = 'confirm'
        deleteContact(ussd_body, s_params, mobile_number, 'delete', pg, del_fn, sessionID)


      elsif trace.function == 'lastname' && trace.page == 3 && ussd_body != "00"
        pg = 4
        fn = 'phone_number'
        addContact(ussd_body, s_params, mobile_number, 'phone_num', pg, fn, sessionID )
        
        
      elsif trace.function == 'phone_number' && trace.page == 4 && ussd_body == "00"
        pg = 3
        fn = 'lastname'
        addContact(ussd_body, s_params, mobile_number, 'lname', pg, fn, sessionID )
        
        

      elsif trace.function == 'confirm' && trace.page == 3
        pg = 4
        del_fn = "confirm_delete"
        deleteContact(ussd_body, s_params, mobile_number, 'deleted', pg, del_fn, sessionID)
        

      elsif trace.function == 'phone_number' && trace.page == 4 && ussd_body != "00"
        pg = 5
        fn = 'age'
        addContact(ussd_body, s_params, mobile_number, 'age', pg, fn, sessionID )
        
        
      elsif trace.function == 'phone_number' && trace.page == 4 && ussd_body == "00"
        pg = 3
        fn = 'lastname'
        addContact(ussd_body, s_params, mobile_number, 'fname', pg, fn, sessionID )


      elsif trace.function == 'age' && trace.page == 5 && ussd_body != "00"
        pg = 6
        fn = 'gender'
        addContact(ussd_body, s_params, mobile_number, 'gender', pg, fn, sessionID)
        
        
     elsif trace.function == 'age' && trace.page == 5 && ussd_body == "00"
        pg = 4
        fn = 'phone_number'
        addContact(ussd_body, s_params, mobile_number, 'phone_num', pg, fn, sessionID )


      elsif trace.function == 'gender' && trace.page == 6 && ussd_body != "00"
        pg = 7
        fn = 'done'
        addContact(ussd_body, s_params, mobile_number, 'done', pg, fn, sessionID)
        
        
      elsif trace.function == 'gender' && trace.page == 6 && ussd_body == "00"
        pg = 5
        fn = 'age'
        addContact(ussd_body, s_params, mobile_number, 'age', pg, fn, sessionID)

      elsif trace.function == 'done' && trace.page == 7 && ussd_body != "00"
        pg = 8
        fn = 'inserted'
        addContact(ussd_body, s_params, mobile_number, 'inserted', pg, fn, sessionID)
        
        
     elsif trace.function == 'done' && trace.page == 7 && ussd_body == "00"
        pg = 6
        fn = 'gender'
        addContact(ussd_body, s_params, mobile_number, 'gender', pg, fn, sessionID)

      end

    end

end
